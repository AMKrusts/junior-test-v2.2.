<?php
Class table {
  protected $id = null;
  protected $table = null;
  function __construcor(){
  }

  function bind($data){
    foreach($data as $key=>$value) {
      $this->$key = $value;
    }
  }
//for loading a single object
function load($id){
	$this->id = $id;
	$dbo = database::getInstance();
	$sql = $this->buildQuery('load');
	$dbo->doQuery($sql);
	$row = $dbo->loadObjectList();

	foreach($row as $key=>$value) {
		if($key == 'id') {
        		continue;
		}
		$this->$key = $value;
	}
}
// function for stoing a data in the database
function store() {
	$dbo = database::getInstance();
	$sql = $this->buildQuery('store');
  	$dbo->doQuery($sql);
}
// function to delete all the table
function delete() {
	$dbo = database::getInstance();
	$sql = $this->buildQuery('delete');
	$dbo->doQuery($sql);
}
// function to delete a specific instances
function deleteSpec($toBeDeleted){
	$this->toBeDeleted = $toBeDeleted;
	$dbo = database::getInstance();
	$sql = $this->buildQuery('deletesec');
  	$dbo->doQuery($sql);
}
// function for getting a row count
function getRowsCount(){
	$dbo = database::getInstance();
	$sql = $this->buildQuery('getRowCount');
    	$dbo->doQuery($sql);
	$rowCount = $dbo->loadObjectList();
	return $rowCount;
}
// function for getting all the values from colum with name id
function getColumCount() {
	$dbo = database::getInstance();
	$sql = $this->buildQuery('columCount');
	$dbo->doQuery($sql);
	$columCount = $dbo->loadColumn();
	return $columCount;
}
// function for creating querys
protected function buildQuery($task) {
	$sql = '';
	if($task == 'store') {
		if($this->id == '') {
			$keys = '';
			$values = '';
			$classVars = get_class_vars(get_class($this));
        		$sql .= "INSERT INTO {$this->table}";

        		foreach($classVars as $key=>$value) {
       				if($key == 'id' || $key == 'table') {
        				continue;
        			}
			$keys .= "{$key},";
			$values .= "'{$this->$key}',";
			}
			$sql .= "(".substr($keys, 0, -1).") VALUES (".substr($values, 0, -1).");";
		    } else {
			    $classVars = get_class_vars(get_class($this));
       			    $sql .= "UPDATE {$this->table} SET ";
			    foreach($classVars as $key=>$value){
				if($key == 'id' || $key == 'table') {
        			    continue;
       				}
        	  	$sql .= "{$key} = '{$this->$key}', ";
        		}
		$sql = substr($sql, 0, -2)."WHERE ID = {$this->id};";
	}
	}elseif($task == 'load') {
		$sql = "SELECT * FROM {$this->table} WHERE ID = {$this->id};";
	}elseif($task == 'delete'){
		$sql = "DROP TABLE {$this->table};";
	}elseif($task == 'getRowCount') {
	  $sql = "SELECT count(id) AS total FROM {$this->table};";
	}elseif($task == 'deletesec'){
    $sql .= "DELETE FROM {$this->table} ";
    $sql .= " WHERE ID = {$this->toBeDeleted};";
	}elseif($task == 'columCount') {
	 $sql = "SELECT id FROM {$this->table};";
	}
return $sql;
}
}
?>
