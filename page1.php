<DOCTYPE html>
<html>
  <head>
    <meta charset='utf8'>
	<link rel="stylesheet" type="text/css" href="/css/style.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  </head>
  <body>
   	 <h1> Product List </h1>
	<select class="list">
		<option value=""></option>
		<option value=""></option>
		<option value=""></option>
		<option value=""></option>
	</select>
 <input type="checkbox" id='checkall' /> Select All<br/>

	<hr class="hl"></hr>
<?php
include (__DIR__ . '/phpfiles/index.php');
$adder->loadProducts();
	if(isset($_POST['values'])){
		$toBeDeleted = json_decode($_POST['values']);
		$x = count($toBeDeleted);
		$user = new user();
		while($x != 0){
			$x--;
			$user->deleteSpec($toBeDeleted[$x]);
		}
	}
?>
<script type='text/javascript'>
 $(document).ready(function(){
   // Check or Uncheck All checkboxes
   $("#checkall").change(function(){
     var checked = $(this).is(':checked');
     if(checked){
       $(".check").each(function(){
         $(this).prop("checked",true);
       });
     }else{
       $(".check").each(function(){
         $(this).prop("checked",false);
       });
     }
   });
 
  // Changing state of CheckAll checkbox 
  $(".check").click(function(){
 
    if($(".check").length == $(".check:checked").length) {
      $("#checkall").prop("checked", true);
    } else {
      $("#checkall").removeAttr("checked");
    }

  });
 });
// ajax function for posting the id's of elements to be deleted
function getId(){
	var checked = $('.check:checkbox:checked');
	var i = 0;
	var values = new Array();
	for(key in checked) {
		if(checked.hasOwnProperty(key)){
			var value = checked[key];
			if(value.id == null)
				continue;
			else{
				values[i] = value.id;
				i++;
			}
		}
	}
	var val = JSON.stringify(values);
	$.ajax ({
	type: "POST",
	url: "page1.php",
	data: {'values':val}
	});

}

</script>
  </body>
</html>
